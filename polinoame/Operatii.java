package tp.polinoame;
/**
 * Interfata ce defineste toate operatiile pe un monom
 * @author olarp
 *
 */
public interface Operatii {

	public Polinom adunare(Polinom p);

	public Polinom scadere(Polinom p);

	public Polinom  inmultire(Polinom p);

	public Polinom derivare();

	public Polinom integrare();
}
