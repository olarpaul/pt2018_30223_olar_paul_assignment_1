package tp.polinoame;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Reprezentarea in Java a conceptului matematic de polinom si operatiile pe
 * acesta. Contine o colectie de monoame. Fiecare monom este pus in lista pe
 * pozitia exponentului pe care il reprezinta
 * 
 * @author olarp
 *
 */
public class Polinom implements Operatii {
	// Lista de termeni ai polinomului
	private List<Monom> termeni = new ArrayList<Monom>();

	// Constructor ce va construi un obiect polinom din reprezentarea acestuia in
	// string
	public Polinom(String polinomString) {
		// Folosit pentru a parsa stringul de intrare
		StringTokenizer st;
		// Vom imparti stringul dupa patternul : "* x^"
		// In fata va fi coeficientul in spate exponentul
		st = new StringTokenizer(polinomString, "* x^ ");
		// Parcurgem lista de elemente care s-a format dupa impartire
		while (st.hasMoreTokens()) {
			int exp = 0;
			double coef = 0;
			// Luam coeficientul
			coef = Double.parseDouble(st.nextToken());
			// Verificam daca avem exponent
			if (st.hasMoreTokens()) {
				// Daca da, il extragem
				exp = Integer.parseInt(st.nextToken());
			} else {
				// Daca nu il setam pe 0
				exp = 0;
			}
			// Adaugam termenul nou format
			adaugaTermen(coef, exp);
		}
	}

	public Polinom() {
		// Constructorul implicit
	}

	/**
	 * Adauga in lista de termeni monomul avand coeficientul coef si exponentul exp
	 * 
	 * @param coef
	 * @param exp
	 */
	public void adaugaTermen(double coef, int exp) {
		// Daca avem un exponent mai mare decat size-ul lsitei curente, populam lista cu
		// elemente cu coef 0
		if (exp > termeni.size()) {
			for (int i = 0; i < exp; i++) {
				termeni.add(new Monom(0, i));
			}
		}
		// Updatam coeficientul elementului de pe pozitita exp
		if (termeni.size() > exp) {
			double coefExistent = termeni.get(exp).getCoef();
			termeni.set(exp, new Monom(coefExistent + coef, exp));
		} else {
			// Elementul nu exista in lista asa ca il vom adauga
			termeni.add(exp, new Monom(coef, exp));
		}
	}

	@Override
	public String toString() {
		String rezultat = "";
		// Parcurgem lista de teremeni de la sfarsit la inceput pentru ca reprezentarea
		// sa fie comnform cu cea matematica
		for (int i = termeni.size() - 1; i >= 0; i--) {
			if (termeni.get(i).getCoef() == 0) {
				rezultat += "";
			} else if (termeni.get(i).getCoef() < 0) {
				rezultat += " - " + termeni.get(i).toString();
			} else if (i < termeni.size() - 1) {
				rezultat += " + " + termeni.get(i).toString();
			} else {
				rezultat += termeni.get(i).toString();
			}
		}
		return rezultat;
	}

	public Polinom adunare(Polinom p) {
		Polinom rezultat = this;
		// Parcurgem lista de termeni ai polinomului p
		for (Monom m : p.termeni) {
			rezultat.adaugaTermen(m.getCoef(), m.getExp());
		}
		return rezultat;
	}

	public Polinom scadere(Polinom p) {
		Polinom rezultat = this;
		for (Monom m : p.termeni) {
			// Adaugam termenul si inmultim coeficientul cu -1
			rezultat.adaugaTermen(m.getCoef() * (-1), m.getExp());
		}
		return rezultat;
	}

	public Polinom inmultire(Polinom p) {
		Polinom rezultat = new Polinom();
		// Parcurgem ambele polinoame
		for (Monom m : this.termeni) {
			for (Monom mp : p.termeni) {
				// Inmultim coef si adunam exp
				rezultat.adaugaTermen(m.getCoef() * mp.getCoef(), m.getExp() + mp.getExp());
			}
		}
		return rezultat;
	}

	public Polinom derivare() {
		Polinom rezultat = new Polinom();
		for (Monom m : this.termeni) {
			// Derviam fiecare termen in parte prin a inmulti coef cu exp si a scadea exp cu
			// -1
			if (m.getExp() - 1 >= 0) {
				rezultat.adaugaTermen(m.getCoef() * m.getExp(), m.getExp() - 1);
			}
		}
		return rezultat;
	}

	public Polinom integrare() {
		Polinom rezultat = new Polinom();
		for (Monom m : this.termeni) {
			// Integram fiecare termen prin a imparti coef la exp+1 si adaugam +1 la exp
			rezultat.adaugaTermen(m.getCoef() / (m.getExp() + 1), m.getExp() + 1);
		}
		return rezultat;
	}
}
