package tp.polinoame;

import java.awt.Dimension;

public class App {
	public static void main(String[] args) {

		GUI g = new GUI();      // create a new object of type GUI
        g.setVisible(true);     // because the GUI class extends JFrame that means that g will act like a JFrame, so we can's see nothing unless we set it's visibility to true
        g.setTitle("Operatii simple pe polinoame");     // set the title of the application window
        g.setPreferredSize(new Dimension(730, 380));    // a dimension which fits with our needs
        g.setResizable(false);  // lock the risize option for window
        g.pack();   // let the window to be sized at the preferred size
        //g.start();
	}

}
