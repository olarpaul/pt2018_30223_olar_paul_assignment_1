package tp.polinoame.test;

import org.junit.Test;

import tp.polinoame.Monom;

public class MonomTest {
	@Test
	public void testGettereMonom() {
		Monom m = new Monom(2, 1);
		
		assert (m.getExp() == 1);
		assert (m.getCoef() == 2);
	}

	@Test
	public void testSettereMonom() {
		Monom m = new Monom(3, 1);
		m.setCoef(2.3d);
		m.setExp(4);
		
		assert (m.getCoef() == 2.3d);
		assert (m.getExp() == 4);
	}

	@Test
	public void testMonoameEgale() {
		Monom m1 = new Monom(2, 1);
		Monom m2 = new Monom(2, 1);

		assert (m1.equals(m2));
	}
	
	@Test
	public void testMonomToString() {
		Monom m1 = new Monom(1,4);
		Monom m2 = new Monom(0,3);
		Monom m3 = new Monom(3,4);
		
		assert(m1.toString().equals("x^4"));
		assert(m2.toString().equals(""));
		assert(m3.toString().equals("3*x^4"));
	}
}
