package tp.polinoame.test;

import org.junit.Test;

import tp.polinoame.Polinom;

public class PolinomTest {

	@Test
	public void testPolinomAdaugaTermen() {
		Polinom p = new Polinom();
		p.adaugaTermen(3, 1);
		p.adaugaTermen(2, 2);

		assert (p.toString().equals("2*x^2 + 3*x"));
	}

	@Test
	public void testConstructorPolinomString() {
		Polinom p = new Polinom("2*x^3 +3*x^2 -4*x^1");

		assert (p.toString().equals("2*x^3 + 3*x^2 - 4*x"));
	}

	@Test
	public void testAdunarePolinom() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		p1.adaugaTermen(1, 2);
		p1.adaugaTermen(1, 1);
		p2.adaugaTermen(2, 2);
		p2.adaugaTermen(2, 1);

		assert (p1.adunare(p2).toString().equals("3*x^2 + 3*x"));
	}

	@Test
	public void testScaderePolinom() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		p1.adaugaTermen(1, 2);
		p1.adaugaTermen(1, 1);
		p2.adaugaTermen(2, 2);
		p2.adaugaTermen(2, 1);

		assert (p1.scadere(p2).toString().equals(" - 1*x^2 - 1*x"));
	}

	@Test
	public void testInmultirePolinom() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		p1.adaugaTermen(1, 1);
		p1.adaugaTermen(1, 0);
		p2.adaugaTermen(1, 1);
		p2.adaugaTermen(1, 0);

		assert (p1.inmultire(p2).toString().equals("x^2 + 2*x + 1"));
	}

	@Test
	public void testIntegrarePolinom() {
		Polinom p = new Polinom();
		p.adaugaTermen(1, 1);
		p.adaugaTermen(1, 2);
		p.adaugaTermen(1, 0);

		assert (p.integrare().toString().equals("0.3333*x^3 + 0.5*x^2 + 1*x"));
	}

	@Test
	public void testDerviarePolinom() {
		Polinom p = new Polinom();
		p.adaugaTermen(1, 1);
		p.adaugaTermen(1, 2);
		p.adaugaTermen(1, 0);

		assert (p.derivare().toString().equals("2*x + 1"));
	}

}
