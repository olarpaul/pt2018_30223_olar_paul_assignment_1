package tp.polinoame;

import java.text.DecimalFormat;

/**
 * Obiectul de baza folosit in construierea unui polinom. Un polinom va contine
 * o colectie de astfel de obiecte
 * 
 * @author olarp
 *
 */
public class Monom {
	// Folosit pentru formatare coeficient
	private static final DecimalFormat FORMATTER = new DecimalFormat("#.####");

	private double coef;
	private int exp;

	/**
	 * Constructor care are nevoie de coeficient si exponent
	 * @param coef
	 * @param exp
	 */
	public Monom(double coef, int exp) {
		this.coef = coef;
		this.exp = exp;
	}

	public double getCoef() {
		return coef;
	}

	public void setCoef(double coef) {
		this.coef = coef;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	@Override
	public String toString() {
		// Formatam coeficientul din cauza ca este double si nu vrem sa apara toate
		// zecimalele
		String coefFormatat = FORMATTER.format(Math.abs(coef));
		// Verificam cazuri speciale de afisare a monomului
		if (exp == 0 && coef != 0) {
			return coefFormatat;
		} else if (exp == 1 && coef != 0) {
			return coefFormatat + "*x";
		} else if (coef == 1) {
			return "x^" + exp;
		} else if (coef == 0) {
			return "";
		} else {
			return coefFormatat + "*x^" + exp;
		}
	}

	@Override
	public boolean equals(Object monom) {
		Monom deComparat = (Monom) monom;
		// Un momon este egal cu alt monom daca atat coeficientul cat si exponentul sunt
		// egale
		return coef == deComparat.coef && exp == deComparat.exp;
	}
}
