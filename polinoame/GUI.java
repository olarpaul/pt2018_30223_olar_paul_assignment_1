package tp.polinoame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class GUI extends JFrame {
	// continutul ferestrei aplicatiei
	private JButton btnOk1, btnOk2;
	private JButton btnAdd, btnSub, btnMul, btnDer, btnInteg;
	private JTextField txtP1;
	private JTextField txtP2;
	private JTextArea areaRez;
	private JLabel lbl1, lbl2, lblTitlu, lblOper, lblRez;
	private JPanel pan11, pan12, pan21, pan22, pan3, pan4, pan5, pan0;
	private Polinom p1, p2;
	private JScrollPane sp;

	public GUI() {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		lblTitlu = new JLabel("Operatii cu polinoame");
		pan11 = new JPanel();
		pan12 = new JPanel();
		pan21 = new JPanel();
		pan22 = new JPanel();
		pan3 = new JPanel();
		pan4 = new JPanel();
		pan5 = new JPanel();
		pan0 = new JPanel();
		lblTitlu.setForeground(Color.white);
		pan11.setBackground(Color.black);
		pan11.add(lblTitlu);
		pan11.setLayout(new FlowLayout(FlowLayout.LEFT));
		btnAdd = new JButton("Adunare");
		btnSub = new JButton("Scadere");
		btnMul = new JButton("Inmultire");
		btnDer = new JButton("Derivata");
		btnInteg = new JButton("Integrare");
		btnOk1 = new JButton("OK");
		btnOk2 = new JButton("OK");
		txtP1 = new JTextField("2*x^3 +3*x^2 -4*x^1", 50);
		txtP2 = new JTextField("1*x^4 -5*x^2 +2", 50);
		lbl1 = new JLabel("Polinomul 1: ");
		lbl2 = new JLabel("Polinomul 2: ");
		lblOper = new JLabel("Operatii ");
		lblRez = new JLabel("Rezultate: ");
		areaRez = new JTextArea(10, 50);
		pan12.add(lbl1);
		pan12.add(txtP1);
		pan12.add(btnOk1);
		pan22.add(lbl2);
		pan22.add(txtP2);
		pan22.add(btnOk2);
		pan21.add(lblOper);
		pan21.add(btnAdd);
		pan21.add(btnSub);
		pan21.add(btnMul);
		pan21.add(btnDer);
		pan21.add(btnInteg);
		pan21.setLayout(new FlowLayout());
		pan4.setLayout(new BorderLayout());
		pan4.add(lblRez, BorderLayout.NORTH);
		pan5.add(areaRez);
		sp = new JScrollPane(areaRez, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pan5.add(sp);
		pan3.setLayout(new BorderLayout());
		pan3.add(pan4, BorderLayout.WEST);
		pan3.add(pan5, BorderLayout.CENTER);
		pan0.add(pan11);
		pan0.add(pan12);
		pan0.add(pan22);
		pan0.add(pan21);
		pan0.add(pan3);
		this.add(pan0);

		// am dezactivat butoanele care nu pot fi folosite de la inceputul aplicatiei
		btnAdd.setEnabled(false);
		btnDer.setEnabled(false);
		btnMul.setEnabled(false);
		btnSub.setEnabled(false);
		btnOk2.setEnabled(false);
		btnInteg.setEnabled(false);

		class ButtonListen implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == btnOk1) { // cazul in care butonul de "Ok" pentru primul polinom este apasat
					p1 = new Polinom(txtP1.getText()); // creaza un nou polinom
					areaRez.append("P1(x) = " + p1.toString() + "\n");
					btnOk2.setEnabled(true); // activam al doilea buton de "Ok"
					btnDer.setEnabled(true); // activam butoanele pentru integrare si derivare
					btnInteg.setEnabled(true);
				} else if (e.getSource() == btnOk2) { // cazul in care cel de-al doilea buton este apasat.
					p2 = new Polinom(txtP2.getText());
					areaRez.append("P2(x) = " + p2.toString() + "\n");
					btnAdd.setEnabled(true); // activam butoanele pentru operatiile aritmetice care se realizeaza cu
												// doua polinoame
					btnMul.setEnabled(true);
					btnSub.setEnabled(true);
				} else if (e.getSource() == btnDer) { // cazul in care userul decide sa aleaga operagtia de derivare

					areaRez.append("P1'(x) = " + p1.derivare().toString() + "\n");
				} else if (e.getSource() == btnInteg) {// cazul in care userul decide sa aleaga operagtia de integrare
					areaRez.append("P1'(x) = " + p1.integrare().toString() + "\n");
				} else {
					if (e.getSource() == btnAdd) { // cazul in care userul decide sa aleaga operagtia de adunare
						Polinom p3 = p1.adunare(p2); // apelam metoda de adunare

						areaRez.append("P1(x) + P2(x) = " + p3.toString() + "\n");
					} else if (e.getSource() == btnSub) { // cazul pentru care userul decide sa apese butonul pt
															// operatia de scadere
						Polinom p3 = p1.scadere(p2);

						areaRez.append("P1(x) - P2(x) = " + p3.toString() + "\n");
					} else if (e.getSource() == btnMul) { // cazul in care se realizeaza inmultirea
						Polinom p3 = p1.inmultire(p2);

						areaRez.append("P1(x) * P2(x) = " + p3.toString() + "\n");
					}
				}
			}
		}
		// daca nu asociem un "ActionListener" butoanelor, acestea nu vor functiona
		// deloc
		btnOk1.addActionListener(new ButtonListen());
		btnOk2.addActionListener(new ButtonListen());
		btnDer.addActionListener(new ButtonListen());
		btnAdd.addActionListener(new ButtonListen());
		btnSub.addActionListener(new ButtonListen());
		btnMul.addActionListener(new ButtonListen());
		btnInteg.addActionListener(new ButtonListen());
	}

}
